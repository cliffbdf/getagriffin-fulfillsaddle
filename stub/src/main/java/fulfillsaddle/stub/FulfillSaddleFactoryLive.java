package fulfillsaddle.stub;

import cliffberg.utilities.HttpUtilities;

import com.google.gson.GsonBuilder;
import com.google.gson.Gson;

import java.net.URL;

public class FulfillSaddleFactoryLive implements FulfillSaddleFactory {

	public FulfillSaddleFactoryLive() {}

	public FulfillSaddleStub createFulfillSaddle() throws Exception {

		return new FulfillSaddleStub() {
			/* Live implementations */

			private String serviceURL;
			private boolean reqsEnabled = true;

			{
				this.serviceURL = System.getenv("FULFILL_SADDLE_SERVICE_URL");
				if (serviceURL == null) throw new RuntimeException(
					"FULFILL_SADDLE_SERVICE_URL not set in environment");
			}

			public String requisitionFromInventory(int deptNo) throws Exception {
				if (! this.reqsEnabled) throw new Exception("No saddles available");
				String jsonStr = HttpUtilities.getHttpResponseAsString(
					new URL(this.serviceURL + "/RequisitionFromInventory?DeptNo=" + deptNo));
				RequisitionResponse orderResponse = parseResponse(jsonStr);
				String saddleId = orderResponse.getSaddleId();
				if ((saddleId == null) || saddleId.equals("")) {
					Exception ex = new RuntimeException("Internal server error: empty saddle ID received from server");
					System.err.println("Response from fulfillsaddle was: " + jsonStr);
					throw ex;
				}

				return saddleId;
			}

			public void addToInventory(String saddleId) throws Exception {
				/* Call the FulfillSaddle microservice at the CancelRequisition endpoint: */
		        URL url = new URL(this.serviceURL + "/AddToInventory?SaddleId=" + saddleId);
				HttpUtilities.getHttpResponseAsString(url);
			}

			public void cancelRequisition(String saddleId) throws Exception {
				URL url = new URL(this.serviceURL + "/CancelRequisition?SaddleId=" + saddleId);
				HttpUtilities.getHttpResponseAsString(url);
			}

			public void haltRequisitions() throws Exception {
				this.reqsEnabled = false;
			}

			public void enableRequisitions() throws Exception {
				this.reqsEnabled = true;
			}

			public String ping() throws Exception {
				return HttpUtilities.getHttpResponseAsString(
					new URL(this.serviceURL + "/ping"));
			}
		};
	}

	static class RequisitionResponse {
		public String saddleId;
		public RequisitionResponse(String saddleId) { this.saddleId = saddleId; }
		public void setSaddleId(String saddleId) { this.saddleId = saddleId; }
		public String getSaddleId() { return this.saddleId; }
	}

	private RequisitionResponse parseResponse(String jsonStr) throws Exception {

		GsonBuilder builder = new GsonBuilder();
		Gson gson = builder.create();
		RequisitionResponse requisitionResponse;
		try {
			requisitionResponse = gson.fromJson(jsonStr, RequisitionResponse.class);
		} catch (Exception ex) {
			throw new RuntimeException("Ill-formatted JSON server response: " + jsonStr);
		}
		return requisitionResponse;
	}
}
