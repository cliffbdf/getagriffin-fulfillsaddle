FROM openjdk:14
COPY ./fulfillsaddle-main-1.0.jar /
CMD ["java", "-cp", "fulfillsaddle-main-1.0.jar", "fulfillsaddle.main.FulfillSaddle"]
