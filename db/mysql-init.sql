use FulfillSaddle;

DROP TABLE IF EXISTS `FulfillSaddle`;
CREATE TABLE `FulfillSaddle` (
  `SaddleId`	varchar(255) NOT NULL,
  `DeptNo`		bigint(20),
  PRIMARY KEY (`SaddleId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
INSERT INTO FulfillSaddle VALUES ('009', NULL);
INSERT INTO FulfillSaddle VALUES ('010', NULL);
INSERT INTO FulfillSaddle VALUES ('011', NULL);
INSERT INTO FulfillSaddle VALUES ('012', NULL);
INSERT INTO FulfillSaddle VALUES ('013', NULL);
INSERT INTO FulfillSaddle VALUES ('014', NULL);
INSERT INTO FulfillSaddle VALUES ('015', NULL);
INSERT INTO FulfillSaddle VALUES ('016', NULL);
