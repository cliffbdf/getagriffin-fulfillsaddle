@Happy
Feature: Basic

	Scenario: Add to Inventory

		Given that the database does not contain "saddle-abc"
		When I call AddToInventory for "saddle-abc"
		Then a subsequent call to AddToInventory for "saddle-abc" will return an error

	Scenario: Requisition

		Given that the database contains only "saddle-def"
		When I call RequisitionFromInventory with department number 123
		Then it returns SaddleID "saddle-def"
